class Table {
    constructor(init) {
        this.init = init;
    }

    createHeader(data) {
        let open = '<thead><tr>';
        let close = '</tr></thead>';

        data.forEach((el) => {
            open = open + `<td>${el}</td>`;
        });

        return open + close;
    }

    createBody(data) {
        let open = '<tbody>';
        let close = '</tbody>';

        data.forEach((el) => {
            open = open + '<tr>';
            for (let i = 0; i < el.length; i++) {
                open = open + `<td>${el[i]}`;
            }
            open = open + '</tr>';
        });

        return open + close;
    }

    render(element) {
        let table = "<table class='table table-hover'>" + this.createHeader(this.init.columns) + this.createBody(this.init.data) + '</table>';
        element.innerHTML = table;
    }
}

const jsonData = {
    columns: ['Name', 'Email', 'Phone Number'],
    data: [
        ['John', 'john@example.com', '(353) 01 222 3333'],
        ['Mark', 'mark@gmail.com', '(01) 22 888 4444'],
        ['John', 'john@example.com', '(353) 01 222 3333'],
        ['Mark', 'mark@gmail.com', '(01) 22 888 4444'],
    ],
};
const table = new Table(jsonData);

const app = document.getElementById('app');
table.render(app);